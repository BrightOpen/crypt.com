# Why?

Protect the right to privacy and free speech - for **you** and for **everyone** - by encrypting your data and communication. 

Be in charge of your digital life. Those who seek shall find - guidance and support. **сяурт.com** will focus on solutions for folks who are not deeply into information technology or cryptography in particular. There shall be best practices, how-to's, tool and service recommendations suitable for general public.

## I'm a law obeying citizen. Why bother?

It is a very common argument from unsuspecting and well meaning people - "I'm not doing anything wrong, I have nothing to hide."

Large online service providers (e-mail, social networking...) regularly (ab)use your (big) data for profit. This is how you pay for their "free" service. 
They have also infamously allowed government agencies to spy on all people indiscriminately (without court warrant or order).
There have been many cases of big corporations failing to protect you adequately. You got your accounts and private information hacked or leaked.
The situation is also getting grimmer by the day with surveillance projects like the China's *social credit*.
Without resistance, this model will be implemented in more "democratic" countries as well, if not by force, then as a "convenience".
Consider, who is not on FB, Gogl, Twtr, MS, Apl today? Can you imagine being declined a mortgage because of your online activity? This is happening today.
Will you wait till you are completely under control of corporations and state agencies to resist this slow but determined removal of our liberties? When is "too late"? How much is "too much"?

## Why for everyone?

By encrypting your data, you protect others as well - their information may be on your device. 
By mastering the skills, you will contribute to the general privacy and security awareness shift. 
Last but not least, if many people encrypt, the use of encryption alone will no longer identify vulnerable targets.

## Is this for the child abusers and smugglers?

Cryptography, like any other technology, does not have an opinion. It will be used by wicked people to do wicked things regardless of your using them for the good or not. Wide spread cryptography usage will make it harder for law enforcement bodies to go after undesired activity, be it human traficking or **investigative journalism**. The net effect should be on the positive side. Would you give up using curtains and window blinds to make law enforcement easier, for the same reason that bad things may happen out of sight?

# Own your data

Nobody shall modify or access your data without your consent. Encryption is a reliable tool to ensure that this is the case.

1. Full disk encryption on your PC
2. Password manager

# Own your communication

Cryptography enables you to have private conversation with your peers online. It may provide even higher protection - [deniable encryption](https://en.wikipedia.org/wiki/Deniable_encryption).

There are services which do not abuse your custom. Or better yet, solutions you can run on your own. When considering the costs of doing so - fees and effort, judge against the real cost of the "free" services. What do we sacrifice for the free subscription? Many viable privacy enabling solutions are free of charge. Some may be hard to master or not widely adopted. Don't let that deter you and accept the challenge.

1. Search
    * [StartPage.com](https://www.startpage.com/)
2. E-mail
    * [StartMail.com](https://www.startmail.com/en/)
3. Social network - famliy, friends, colleagues and their stories
    * [Mastodon](https://joinmastodon.org/)
4. Chat - messages, pics, audio
    * [Delta.chat](https://delta.chat)
5. Meeting - audio, video, screen sharing
    * [Meet.jit.si](https://meet.jit.si/)
6. Browsing - world wide web
    * [Firefox](https://www.mozilla.org/en-US/firefox/)
    * [HTTPS everywhere](https://www.eff.org/https-everywhere)
7. Collaboration - docs, files, mails, calendars...
    * [Nextcloud](https://nextcloud.com/)
